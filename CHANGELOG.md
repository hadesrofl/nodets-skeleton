# Changelog

## Version 1.0

* Init of project structure
* Minimal running prototype (show table of printer entities)
* CORS enabled
* added gitlab-ci