# MEAN TS Skeleton

## Requirements

* Node > 8.9.4
* MongoDB (or at least a server with it)
	* Locally:
	```bash
	docker run --rm -d \
	--name mean-mongo \
	-e MONGO_INITDB_ROOT_USERNAME=mongoadmin \
	-e MONGO_INITDB_ROOT_PASSWORD=veryInsecurePW \
	-v /path/to/git/mean-ts-skeleton/dev-mongo:/data/db \
	mongo
	```
	* Server:
	`ssh -L 27017:localhost:27017 remote-user@212.83.56.107`
* Webserver (Nginx, Apache) for production

## Use Node-Debugger with VSCode

If you use this boilerplate with VSCode you can run the debugger for node with ```npm run dev``` and use the feature in the bottom line 'auto attach' when running 'Attach to NPM'. This auto attaches the debugger to the running npm process.

## Run the app

To run the app in dev mode you need to run ```npm run dev```. The application will then run on localhost:4200 for the angular app and localhost:3000 for the node backend. These commands will rebuild the nodets and ng application on changed files. **Be aware that the backend server only starts when a debugger is attached to it**.

## ApiDocs

Run ```npm run apidocs``` to create the latest api in the directory *docs*. Please not that if you are not using the frontend to access the backend, you need to set *cors* to undefined in api/config.ts.

## Tests

For tests to run, there needs to be a database hooked to the project. Check out the api/config.ts.