import { environment } from '../../environments/environment';
const backendUrl = !environment.production ? 'http://localhost:3000/api/v1' : 'http://localhost:3000/api/v1';

export const routes = {
    backendUrl: backendUrl,
    paths: {
        backend: {
            machines: {
                root: 'machines',
                byId: ':type/:id',
                getAll: '',
                machineTypes: 'types',
                materials: 'materials',
                laserTypes: 'laserTypes',
                count: 'count'
            }
        },
        frontend: {
            machines: {
                root: 'machines',
                create: 'create',
                update: 'update',
                delete: 'delete',
                getById: ':type/:id',
                getAll: '',
                machineTypes: 'types',
                materials: 'materials',
                laserTypes: 'laserTypes'
            }
        },
    }
};



