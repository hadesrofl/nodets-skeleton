import { MachineListComponent } from '../machines/machine-list/machine-list.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { Routes } from '@angular/router';
import { routes } from './routes';

export const appRoutes: Routes = [
    {
        path: routes.paths.frontend.machines.root,
        component: MachineListComponent,
    },
    {
        path: '',
        component: DashboardComponent
    }
];
