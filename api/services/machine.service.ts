import { Printer } from '../models/machines/printer.model';

/**
 * This method gets a specific type of machine (or all) and returns a promise with the results
 * type is the type of machine to get
 * limit is the number of items to get
 * skip is the number of items to skip
 * @returns a promise with the results
 */
function getMachineType (type: String, limit?: Number, skip?: Number) {
  const promises = [];
  let obj;
  switch (type) {
    case 'printer':
      obj = [];
      if (limit >= 0 && skip >= 0) {
        promises.push(Printer.find().limit(limit).skip(skip));
      } else {
        promises.push(Printer.find());
      }
      break;
    default:
      obj = {
        printers: [],
        lasercutters: [],
        millingMachines: [],
        otherMachines: []
      };
      promises.push(Printer.find());
      break;
  }
  return Promise.all(promises).then((results) => {
    if (results) {
      results.forEach((machines) => {
        machines.forEach((machine) => {
          if (Array.isArray(obj)) {
            obj.push(machine);
          } else {
            Object.keys(obj).forEach((type) => {
              if (type.startsWith(machine.type)) {
                obj[type].push(machine);
              }
            });
          }
        });
      });
      return obj;
    }
    return [];
  });
}

/**
 * This method creates a specific type of machine and returns a promise with the results
 * type is the type of machine to create
 * params are the params of the machine
 * @returns a promise with the results
 */
function create (type, params) {
  if (!params.type) {
    params.type = type;
  }
  switch (type) {
    case 'printer':
      return (new Printer(params)).save();
    default:
      return Promise.reject('Machine Type not supported!');
  }
}

/**
 * This method deletes a specific type of machine by a given id and returns a promise with the success or failure
 * type is the type of machine to delete
 * _id is the id of the machine
 * @returns a promise with the results
 */
function deleteById (type, _id) {
  switch (type) {
    case 'printer':
      return Printer.deleteOne({ _id });
    default:
      return Promise.reject('Machine Type not supported!');
  }
}

/**
 * This method gets a specific type of machine by a given id and returns a promise with the result
 * type is the type of machine to get
 * _id is the id of the machine
 * @returns a promise with the results
 */
function get (type, _id) {
  switch (type) {
    case 'printer':
      return Printer.findOne({ _id });
    default:
      return Promise.reject('Machine Type not supported!');
  }
}

/**
 * This method updates a specific type of machine by a given id and returns a promise with the result
 * type is the type of machine to update
 * _id is the id of the machine
 * machine is the machine with updated fields
 * @returns a promise with the result
 */
function update (type, _id, machine) {
  delete machine.__v;
  switch (type) {
    case 'printer':
      return Printer.update(
        { _id },
        machine,
        { upsert: true }).then(() => Printer.findOne({ _id }));
    default:
      return Promise.reject('Machine Type not supported!');
  }
}

/**
 * This method counts a specific type of machine returns a promise with the result
 * type is the type of machine to count
 * @returns a promise with the result
 */
function count (type) {
  switch (type) {
    case 'printer':
      return Printer.countDocuments();
    default:
      return Promise.reject('Machine Type not supported!');
  }
}

export default {
  getMachineType,
  create,
  deleteById,
  get,
  update,
  count
};
