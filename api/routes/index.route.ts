import * as express from 'express';
import machineRoute from './machine.route';

const router = express.Router();

/**
 * @api {get} /api/v1/ Gets a health check on the backend
 * @apiName GetHealth
 * @apiVersion 1.0.0
 * @apiGroup Meta
 * @apiHeader (Needed Request Headers) {String} Content-Type application/json
 *
 * @apiSuccess {Object} health 'alive'
 *
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 200 OK
{
  "health": "alive"
}
 */
router.get('/', (req, res) => {
  res.send({ health: 'alive' });
});

/**
 * @api {get} /api/v1/version Get version number of the software
 * @apiName GetVersion
 * @apiVersion 1.0.0
 * @apiGroup Meta
 * @apiHeader (Needed Request Headers) {String} Content-Type application/json
 *
 * @apiSuccess {Object} version number
 *
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 200 OK
{
  "version": "1.0.0"
}
 */
router.get('/version', (req, res) => {
  res.send({ version: process.env.npm_package_version });
});

router.use('/machines/', machineRoute);

export default router;
