import * as express from 'express';
import printerRoute from '../routes/printer.route';

const router = express.Router();

router.use('/printers/', printerRoute);

export default router;
